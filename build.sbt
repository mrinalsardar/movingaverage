name := "Moving Average"

version := "0.1"

scalaVersion := "2.11.3"

mainClass := Some("com.mrinalksardar.movingaverage.MovingAverage")

libraryDependencies ++=
    "org.apache.spark" %% "spark-core" % "2.2.0" ::
    Nil
